<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    // USER CONTROLLER
    public function index()
    {
        $title = "All Users";
        $data = [
            'title' => $title,
            'count' => count(User::all())
        ];
        return view('user', $data);
    }
}
