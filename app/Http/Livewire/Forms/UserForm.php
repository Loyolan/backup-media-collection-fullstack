<?php

namespace App\Http\Livewire\Forms;

use Livewire\Component;
use App\Models\User;

class UserForm extends Component
{
    public User $user;

    protected $rules = [
        'user.name' => 'required|string|min:4',
        'user.email' => 'required|email'
    ];

    public function save()
    {
        $this->validate();
        $this->user->save();
        $this->emit('userUpdated');
    }

    public function render()
    {
        return view('livewire.forms.user-form');
    }
}
