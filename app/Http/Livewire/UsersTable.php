<?php

namespace App\Http\Livewire;

use App\Models\User;

use Livewire\Component;
use Livewire\WithPagination;

class UsersTable extends Component
{
    use WithPagination;

    public string $search = '';

    public string $orderField = 'name';
    public string $orderDirection = 'ASC';

    public int $editId = 0;
    
    protected $queryString = [
        'search' => ['except' => ''],
        'orderField' => ['except' => 'name'],
        'orderDirection' => ['except' => 'ASC']
    ];

    protected $listeners = [
        'userUpdated' => 'onUserUpdated'
    ];

    public array $selection = [];

    // SET ORDERED AFTER CLICKING TH
    public function setOrderField(string $name) 
    {
        if($name === $this->orderField) {
            $this->orderDirection = $this->orderDirection === 'ASC' ? 'DESC' : 'ASC';
        } else {
            $this->orderField = $name;
            $this->reset('orderDirection');
        }
    }

    // SHOW EDIT FORM FOR EACH USER
    public function startEdit(int $id)
    {
        if($this->editId === $id){
            $this->editId = 0;
        } else {
            $this->editId = $id;
        }
    }

    // UPDATE PAGE AFTER EDITING
    public function onUserUpdated()
    {
        $this->reset('editId');
    }

    // DELETION MULTIPLE
    public function deleteUsers(array $ids)
    {
        User::destroy($ids);
        $this->selection = [];
    }

    public function updating($name, $value)
    {
        if($name === 'search'){
            $this->resetPage();
        }
    }

    public function render()
    {
        return view('livewire.users-table', [
            'users' => User::where('name', 'LIKE', "%{$this->search}%")
            ->orderBy($this->orderField, $this->orderDirection)
            ->paginate(7)
        ]);
    }
}
