<?php

namespace App\Models;

use Ramsey\Uuid\Uuid as RamseyUuid;
use Illuminate\Database\Eloquent\Model;

class Music extends Model
{
    protected $keyType = 'string';
    protected $casts = [
        'id' => 'string',
    ];

    public $incrementing = false;

    protected $fillable = ['title','artist','album', 'gender', 'description', 'location'];

    public static function boot()
    {
        parent::boot();
        static::creating(function($obj) {
            $obj->id = RamseyUuid::uuid4()->toString();
        });
    }
}
