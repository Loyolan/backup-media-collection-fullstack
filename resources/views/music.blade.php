{{--  --}}
@extends('layout.main')
@extends('layout.link')
@extends('layout.sidebar')
@extends('layout.navbar')

@section('sidebar')
    @parent
@stop
@section('content')
    <div class="mt-5 p-3" x-data="musicUnit({{Js::from($musics)}})">
        <div class="grid grid-cols-5 gap-5">
            @foreach($musics as $music)
                <x-tools.card-music :data="$music" :value="$music->id" x-model="id"/>
            @endforeach
        </div>
    </div>
    <script src="{{ asset('js/music-script.js') }}"></script>
@stop