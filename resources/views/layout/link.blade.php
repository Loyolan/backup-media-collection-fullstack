@section('link')
        <link rel="preconnect" href="https://fonts.bunny.net">
        <!-- <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;600;700&display=swap" rel="stylesheet"> -->
        {{-- <link rel="stylesheet" type="text/css" href="{{ asset('css/tailwind.min.css') }}"> --}}
        <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-icon/bootstrap-icons.css') }}">
        <script src="//unpkg.com/alpinejs" defer></script>
        <script src="https://cdn.tailwindcss.com"></script>
        {{-- <script src="{{ asset('js/alpine.min.js') }}" defer></script>--}}
        <script src="{{ asset('js/app.js') }}" defer></script>
@endsection