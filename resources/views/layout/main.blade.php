<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/png" sizes="32x32" href="https://flowbite.com/docs/images/logo.svg"/>
        <title>{{ $title }} || Backup media collections</title>

        <!-- Fonts -->
        @yield('link')
        @livewireStyles
    </head>
    <body class="bg-gray-100">
        <div class="container">
            @yield('sidebar')
            <div class="pl-300">
                @yield('navbar')
                @yield('content')
            </div>
        </div>
        @livewireScripts
    </body>
</html>