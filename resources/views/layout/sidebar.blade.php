@section('sidebar')
            <div class="sidebar fixed top-0 bottom-0 lg:left-0 p-2 w-[300px] overflow-y-auto text-center bg-gray-900">
                <div class="text-gray-100 text-x1">
                    <div class="p2.5 mt-1 flex items-center">
                        <i class="bi bi-app-indicator"></i>
                        <h1 class="font-bold text-gray-200 text-[15px] ml-3">
                            Back'Up media Collection
                        </h1>
                        <i class="bi bi-x ml-10 cursor-pointer lg:hidden"></i>
                    </div>
                    <hr class="my-2 text-gray-600">
                </div>
                <div class="p2.5 mt-3 flex items-center rounded-md px-4 duration-300 py-3 cursor-pointer bg-gray-700 text-white">
                    <i class="bi bi-search text-sm"></i>
                    <input type="text" class="text-[15px] ml-4 w-full bg-transparent text-white focus:outline-none" placeholder="Search" name="">
                </div>
                <a href="{{ route('all-musics') }}" class="p2.5 mt-3 flex items-center rounded-md px-4 duration-300 py-3 cursor-pointer hover:bg-blue-600 text-white">
                    <i class="bi bi-music-note-list text-sm"></i>
                    <span class="text-[15px] ml-2">All music</span>
                </a>
                <div class="p2.5 mt-3 flex items-center rounded-md px-4 duration-300 py-3 cursor-pointer hover:bg-blue-600 text-white">
                    <i class="bi bi-person-lines-fill text-sm"></i>
                    <span class="text-[15px] ml-2">Artist</span>
                </div>
                <div class="p2.5 mt-3 flex items-center rounded-md px-4 duration-300 py-3 cursor-pointer hover:bg-blue-600 text-white">
                    <i class="bi bi-list-columns text-sm"></i>
                    <span class="text-[15px] ml-2">Playlist</span>
                </div>
                <div class="p2.5 mt-3 flex items-center rounded-md px-4 duration-300 py-3 cursor-pointer hover:bg-blue-600 text-white">
                    <i class="bi bi-play-btn text-sm"></i>
                    <span class="text-[15px] ml-2">Movies</span>
                </div>
                <div class="p2.5 mt-3 flex items-center rounded-md px-4 duration-300 py-3 cursor-pointer hover:bg-blue-600 text-white">
                    <i class="bi bi-star text-sm"></i>
                    <span class="text-[15px] ml-2">Favoris</span>
                </div>
                <hr class="my-2 text-gray-600">
                <a href="{{ route('all-users') }}" class="p2.5 mt-3 flex items-center rounded-md px-4 duration-300 py-3 cursor-pointer hover:bg-green-600 text-white">
                    <i class="bi bi-people text-sm"></i>
                    <span class="text-[15px] ml-2">Administration</span>
                </a>
                <div class="p2.5 mt-3 flex items-center rounded-md px-4 duration-300 py-3 cursor-pointer hover:bg-red-600 text-white">
                    <i class="bi bi-lock text-sm"></i>
                    <span class="text-[15px] ml-2">Deconnexion</span>
                </div>
            </div>
@endsection