@section('navbar')
<nav class="bg-white border-gray-200 dark:bg-gray-900">
    <div class="flex flex-wrap justify-between items-center mx-auto max-w-screen-xl p-4">
        <a href="https://flowbite.com" class="flex items-center">
            <img src="https://flowbite.com/docs/images/logo.svg" class="h-8 mr-3" alt="Flowbite Logo" />
            <span class="self-center text-2xl font-semibold whitespace-nowrap dark:text-white">{{$title}}</span>
        </a>
        <div class="flex items-center">
            <a href="tel:5541251234" class="mr-6 text-sm  text-gray-500 dark:text-white hover:underline">{{ $count }} items</a>
        </div>
    </div>
</nav>
<nav class="fixed bg-gray-50 dark:bg-gray-700 shadow-lg">
    <div class="max-w-screen-xl px-4 py-3 mx-auto">
        <div class="flex items-center">
            <ul class="flex flex-row font-medium mt-0 mr-6 space-x-8 text-sm">
                <li>
                    <a href="#" class="text-gray-900 dark:text-white hover:underline" aria-current="page">All</a>
                </li>
                <li>
                    <a href="#" class="text-gray-900 dark:text-white hover:underline">English</a>
                </li>
                <li>
                    <a href="#" class="text-gray-900 dark:text-white hover:underline">Spanish</a>
                </li>
                <li>
                    <a href="#" class="text-gray-900 dark:text-white hover:underline">French</a>
                </li>
                <li>
                    <a href="#" class="text-gray-900 dark:text-white hover:underline">Malagasy</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<hr class="my-2 text-gray-600 ">
@endsection