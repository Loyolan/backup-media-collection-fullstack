<th {!! $attributes->merge(['class' => "px-6 py-4 cursor-pointer", 'scope' => "col"]) !!} wire:click="setOrderField('{{ $name }}')">
    @if($visible)
        @if($direction === 'ASC')
            <i class="bi bi-sort-alpha-down pr-2 text-red-900"></i>
        @else
            <i class="bi bi-sort-alpha-down-alt pr-2 text-red-900"></i>
        @endif
    @endif
    {{ $label }}
</th>