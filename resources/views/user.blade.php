{{--  --}}
@extends('layout.main')
@extends('layout.link')
@extends('layout.sidebar')
@extends('layout.navbar')

@section('sidebar')
    @parent
@stop
@section('content')
    <div class="mt-5 p-3">
        <div>
            <livewire:users-table />
        </div>
    </div>
@stop