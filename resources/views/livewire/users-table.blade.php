<div x-data="{selection: @entangle('selection').defer}">
            <div class="">
                <div class="p2.5 mt-3 flex items-center rounded-md px-4 duration-300 py-3 cursor-pointer bg-gray-700 text-white">
                    <i class="bi bi-search text-sm"></i>
                    <input type="text" class="text-[15px] ml-4 w-full bg-transparent text-white focus:outline-none" placeholder="Search" wire:model.debounce.500ms="search">
                </div>
                <div class="p2.5 mt-3 relative flex items-center rounded-md px-4 duration-300 py-3 text-white">
                    <button class="absolute animate-pulse right-2 top-17 bg-red-500 hover:bg-red-700 hover:animate-none text-white font-bold py-2 px-4 rounded inline-flex" x-show="selection.length > 0" x-on:click="$wire.deleteUsers(selection)">
                        <i class="bi bi-trash"></i> Delete all selected
                    </button>
                </div>
            </div>

            <table class="min-w-full text-left text-sm font-light w-[70vw]">
                <thead class="border-b font-medium dark:border-neutral-500">
                    <tr>
                        <th></th>
                        <th scope="col" class="px-6 py-4">#</th>
                        <x-tools.table-header :direction="$orderDirection" label="Username" name="name" :field="$orderField" />
                        <x-tools.table-header :direction="$orderDirection" label="Email" name="email" :field="$orderField" />
                        <th scope="col" class="px-6 py-4">Role</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr class="border-b dark:border-neutral-500">
                        <td>
                            <input id="checked-checkbox" type="checkbox" class="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600" x-model="selection" value="{{ $user->id }}">
                        </td>
                        <td class="whitespace-nowrap px-4 py-2"><p class="px-3 text-center py-2 bg-gray-700 text-white rounded-full">{{ strtoupper(substr($user->name, 0, 1)) }}</p></td>
                        <td class="whitespace-nowrap px-6 py-4">{{ $user->name }}</td>
                        <td class="whitespace-nowrap px-6 py-4">{{ $user->email }}</td>
                        <td class="whitespace-nowrap px-6 py-4">USER</td>
                        <td>
                            <button wire:click="startEdit({{$user->id}})" class="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded inline-flex items-center">
                                <i class="bi bi-pen"></i>
                                <span class="ml-1">Edit</span>
                            </button>
                            <button x-on:click="$wire.deleteUsers([{{$user->id}}])" class="bg-gray-300 hover:bg-gray-400 text-red-500 font-bold py-2 px-4 rounded inline-flex items-center">
                                <i class="bi bi-trash"></i>
                                <span class="ml-1">Delete</span>
                            </button>
                        </td>
                    </tr>
                    @if($editId == $user->id)
                        <tr>
                            <td></td>
                            <td></td>
                            <livewire:forms.user-form :user="$user" :key="$user->id"/>
                            <td></td>
                        </tr>
                    @endif
                @endforeach
                </tbody>
            </table>
            {{ $users->links() }}
</div>
