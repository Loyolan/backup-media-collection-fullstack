<td colspan="3">
    <form action="" wire:submit.prevent="save()">
        <div class="p2.5 mt-3 flex items-center rounded-md px-4 duration-300 py-3 cursor-pointer bg-gray-700 text-white">
            <i class="bi bi-pen text-sm"></i>
            <input type="text" class="text-[15px] ml-4 w-full bg-transparent text-white focus:outline-none" placeholder="Username" wire:model.defer="user.name">
            <i class="bi bi-envelope-at text-sm"></i>
            <input type="text" class="text-[15px] ml-4 w-full bg-transparent text-white focus:outline-none" placeholder="Email" wire:model.defer="user.email">
            <button type="submit" class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded" wire:loading.attr="disabled">
                <i class="bi bi-save"></i>
            </button>
        </div>
        <div class="p2.5 mt-3 grid grid-cols-2 gap-2 flex items-center rounded-md px-4 duration-300 py-1">
            <span wire:loading class="text-green-500 text-xs">Chargement...</span>
            @error('user.name')
            <span wire:loading.remove class="text-red-500 text-xs">{{ $message }}</span>
            @enderror
            @error('user.email')
            <span wire:loading.remove class="text-red-500 text-xs">{{ $message }}</span>
            @enderror
        </div>
    </form>
</td>