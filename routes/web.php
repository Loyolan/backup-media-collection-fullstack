<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MusicController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

$music = "/media/collections/musics";
$user = "/media/collections/users";

Route::get('/', function () {
    return redirect('/media/collections/musics/all');
});

Route::get($music.'/all', [MusicController::class, 'index'])->name('all-musics');

Route::get($user.'/all', [UserController::class, 'index'])->name('all-users');
